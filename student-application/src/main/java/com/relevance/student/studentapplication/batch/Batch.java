package com.relevance.student.studentapplication.batch;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Batch {
	@Id
	String batch_id;
	String batch_name;
	
	public Batch(String batch_id, String batch_name) {
		super();
		this.batch_id = batch_id;
		this.batch_name = batch_name;
	}



	
	
	public Batch()  {}





	public String getBatch_id() {
		return batch_id;
	}





	public void setBatch_id(String batch_id) {
		this.batch_id = batch_id;
	}





	public String getBatch_name() {
		return batch_name;
	}





	public void setBatch_name(String batch_name) {
		this.batch_name = batch_name;
	}





	
	

}

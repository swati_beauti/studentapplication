package com.relevance.student.studentapplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepostiary studentrepostiary;

	public List<Student> getAllStudent() {
		List<Student> student= new ArrayList<>();
		studentrepostiary.findAll().forEach(student::add);
		 return student;
	}

	public Optional<Student> getStudent(String student_id) {
		
		return studentrepostiary.findById(student_id);
	}

	public void addStudent(Student student) {
		studentrepostiary.save(student);
	}

	public void updateStudent(Student student, String student_id) {
		studentrepostiary.save(student);
	}

	public void deleteStudent(String student_id) {
		studentrepostiary.deleteById(student_id);
	}

}

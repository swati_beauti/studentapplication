package com.relevance.student.studentapplication.courseapplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {
	
	@Autowired
	private CourseRepostiary courserepostiary;

	public List<Course> getAllCourse() {
		List<Course> course= new ArrayList<>();
		courserepostiary.findAll().forEach(course::add);
		 return course;
	}

	public Optional<Course> getCourse(String course_id) {
		
		return courserepostiary.findById(course_id);
	}

	public void addCourse(Course course) {
		courserepostiary.save(course);
	}

	public void updateCourse(Course course, String course_id) {
		courserepostiary.save(course);
	}

	public void deleteCourse(String course_id) {
		courserepostiary.deleteById(course_id);
	}

}

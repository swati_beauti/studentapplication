package com.relevance.student.studentapplication.teacher;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TeacherController {
	
	@Autowired
	 private TeacherService teacherservice;
	
	@RequestMapping("/teacher")
	 public List<Teacher> getAllTeacher() {
		return teacherservice.getAllTeacher();
	}
	
	@RequestMapping("/teacher/{teacher_id}")
	public  Optional<Teacher> getTeacher(@PathVariable String teacher_id ) {
		return  teacherservice.getTeacher(teacher_id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/teacher")
	public void addTeacher(@RequestBody Teacher teacher) {
		teacherservice.addStudent(teacher); 
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/teacher/{teacher_id}")
	public void updateTeacher(@RequestBody Teacher teacher,@PathVariable String teacher_id) {
		teacherservice.updateTeacher(teacher,teacher_id);
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/teacher/{teacher_id}")
	 public void deleteTeacher(@PathVariable String teacher_id) {
		teacherservice.deleteTeacher(teacher_id);
	}
	

}

package com.relevance.student.studentapplication.courseapplication;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
//@CrossOrigin(origins = "http://localhost:4200")
@Repository
public  interface CourseRepostiary extends CrudRepository<Course,String>{
	
	public List<Course> findByStudentId(String studentid);
}

package com.relevance.student.studentapplication;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {
	@Id
	String student_id;
	String student_fname;
	String student_lname;
	String student_email;
	public Student(String student_id, String student_fname, String student_lname, String student_email) {
		super();
		this.student_id = student_id;
		this.student_fname = student_fname;
		this.student_lname = student_lname;
		this.student_email = student_email;
	}



	
	
	public Student()  {}


	public String getStudent_id() {
		return student_id;
	}


	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}


	public String getStudent_fname() {
		return student_fname;
	}


	public void setStudent_fname(String student_fname) {
		this.student_fname = student_fname;
	}


	public String getStudent_lname() {
		return student_lname;
	}


	public void setStudent_lname(String student_lname) {
		this.student_lname = student_lname;
	}


	public String getStudent_email() {
		return student_email;
	}


	public void setStudent_email(String student_email) {
		this.student_email = student_email;
	}

}

package com.relevance.student.studentapplication.courseapplication;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.relevance.student.studentapplication.Student;

@Entity
public class Course {
	
	@Id
	String course_id;
	String course_name;
	
	@ManyToMany
	private Student student;
	
	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Course(String course_id, String course_name,String studentid) {
		super();
		this.course_id = course_id;
		this.course_name = course_name;
		this.student=new Student(studentid,"","","");
	}


		public String getCourse_id() {
		return course_id;
	}


	public void setCourse_id(String course_id) {
		this.course_id = course_id;
	}


	public String getCourse_name() {
		return course_name;
	}


	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

}
package com.relevance.student.studentapplication.courseapplication;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.relevance.student.studentapplication.Student;
//@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CourseController {
	
	@Autowired
	 private CourseService courseservice;
	
	@RequestMapping("/student/{id}/course")
	 public List<Course> getAllCourse() {
		return courseservice.getAllCourse();
	}
	
	@RequestMapping("/student/{studentid}/course/{id}")
	public  Optional<Course> getCourse(@PathVariable String id,@PathVariable String studentid ) {
		return  courseservice.getCourse(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/student/{studentid}/course")
	public void addCourse(@RequestBody Course course,@PathVariable String studentid) {
		course.setStudent(new Student(studentid,"","",""));
		courseservice.addCourse(course); 
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/student/{studentid}/course/{id}")
	public void updateCourse(@RequestBody Course course,@PathVariable String studentid,@PathVariable String id) {
		course.setStudent(new Student(studentid,"","",""));
		courseservice.updateCourse(course,id);
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="student/{studentid}/course/{id}")
	 public void deleteCourse(@PathVariable String studentid,@PathVariable String id) {
		courseservice.deleteCourse(id);
	}
	

}

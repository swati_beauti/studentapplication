package com.relevance.student.studentapplication.cclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BatchService {
	
	@Autowired
	private BatchRepostiary batchrepostiary;

	public List<Cclass> getAllBatch() {
		List<Cclass> batch= new ArrayList<>();
		batchrepostiary.findAll().forEach(batch::add);
		 return batch;
	}

	public Optional<Cclass> getBatch(String batch_id) {
		
		return batchrepostiary.findById(batch_id);
	}

	public void addBatch(Cclass batch) {
		batchrepostiary.save(batch);
	}

	public void updateBatch(Cclass batch, String batch_id) {
		batchrepostiary.save(batch);
	}

	public void deleteBatch(String batch_id) {
		batchrepostiary.deleteById(batch_id);
	}

}

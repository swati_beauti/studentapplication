package com.relevance.student.studentapplication;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class StudentController {
	
	@Autowired
	 private StudentService studentservice;
	
	@RequestMapping("/student")
	 public List<Student> getAllStudent() {
		return studentservice.getAllStudent();
	}
	
	@RequestMapping("/student/{student_id}")
	public  Optional<Student> getStudent(@PathVariable String student_id ) {
		return  studentservice.getStudent(student_id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/student")
	public void addStudent(@RequestBody Student student) {
		 studentservice.addStudent(student); 
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/student/{student_id}")
	public void updateStudent(@RequestBody Student student,@PathVariable String student_id) {
		studentservice.updateStudent(student,student_id);
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/student/{student_id}")
	 public void deleteStudent(@PathVariable String student_id) {
		studentservice.deleteStudent(student_id);
	}
	

}

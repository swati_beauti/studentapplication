package com.relevance.student.studentapplication.batch;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class BatchController {
	
	@Autowired
	 private BatchService batchservice;
	
	@RequestMapping("/batch")
	 public List<Batch> getAllBatch() {
		return batchservice.getAllBatch();
	}
	
	@RequestMapping("/batch/{batch_id}")
	public  Optional<Batch> getBatch(@PathVariable String batch_id ) {
		return  batchservice.getBatch(batch_id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/batch")
	public void addBatch(@RequestBody Batch batch) {
		batchservice.addBatch(batch); 
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/batch/{batch_id}")
	public void updateBatch(@RequestBody Batch batch,@PathVariable String batch_id) {
		batchservice.updateBatch(batch,batch_id);
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/batch/{batch_id}")
	 public void deleteBatch(@PathVariable String batch_id) {
		batchservice.deleteBatch(batch_id);
	}
	

}
